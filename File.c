#include <stdio.h>
int main()
{
   FILE *ff;
   char a;
   printf("Enter data:\n");
   ff=fopen("input.dat","w");
   while((a=getchar())!= EOF)
         fputc(a,ff);

   fclose(ff);
   printf("The entered data is: \n");
   ff=fopen("input.dat","r");
   while((a=fgetc(ff))!= EOF)
         putchar(a);
   fclose(ff);
}
